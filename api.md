# API Documentation

## Light Control
A good example of easy use of the light control box. We can simply start and jupyter console and write the following.

```python
import light

l = light.Light()

# Turns all light on
l.all_on()

# Turns all light off.
l.all_off()
```


## Camera Control


## Robot Control


## Projector Control
The projector library is dependent on OpenCV and PyGlet. PyGlet is in charge of creating a second screen and displaying an image on it. It uses OpenCV to stretch the image, and as we currently only use Gray Code we use KNN as interpolation.  
