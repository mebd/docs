# General information about the robot

## Install Procedure
There is currently a bug in FlyCapture2 API v. 2.8.3.1, that prevents multiple camera instances. Because of this we fallback to __FlyCapture2 v. 2.6.3.4__ - which can be downloaded on ptgrey.com website, login needed.

Follow their installation guide provided in their README, as multiple dependencies are needed to be installed beforehand.

For image handling we use __OpenCV v2.4.8__.


For interprocess communication we use [ZeroMQ v. 4.x](http://zeromq.org).
On Ubuntu you must download, compile and install this manually. As of the time of writing this document, the package repository doesn't contain releases newer than 3.x.

As for Python related dependencies needed to be installed beforehand:

- __Python 2.7__
- __jupyter__ (formerly known as iPython)
- __pyglet__
- __Scipy__
- __Numpy__

Numpy and SciPy do have additional dependencies in the form of binaries. It is adviced to install them with `sudo apt-get` or download and compile with pip.


## Software structure
When the SVN is checked out, you'll find a branch and a trunk folder. Due to attempt to upgrade the camera driver only the driver from branch r50 works. The linuxdemo.py already takes care of the confusion.

The structure of the software is divided into the following parts:

- __analysis__ - post processing stuff
- __calicube__ - calibration patterns
- __control__ - control software
- __drivers__ - drivers, mainly camera
- __userscripts__ - Various scripts to ease use of robot
- __utilities__ - Various libraries to help use of robot


## Startup Procedure
Make sure that the cables are correctly and firmly attached:

- Robot controller ethernet cable
- Light box USB 2.0 cable
- 2x Camera USB 3.0 cables.

It is of great importance that the Camera cables are attached to the ptgrey USB 3.0 PCI-express card.

Boot the PC.

Run your script, or the example script given in `robotsuite/userscripts/linuxdemo.py`

That's it!

## Lessons learned

### Controlling the robot
In order to control the robot and perform path planning multiple steps have been considered.

- ABB RobotStudio
- Blender + Python
- Robot Operating System (ROS)
- Python abb package

All solutions provide reasonable solutions but each have many tradeoffs.

#### ABB RobotStudio
ABB RobotStudio provides a 3D model with the inverse kinematics in an interface specifically made for path planning and controlling the robot. You can even import 3D model with complex geometry and make the robot follow it. Another neat feature is the ability to simulate the path and check for collisions before running the robot. This sounds like all we ever need, but it has several drawbacks.

First of all it takes a long time to do the actual planning of the path, this is due to poor GUI design and that if you make a mistake you must delete your path and recreate it from scratch, this is very cumbersome. Also it seems very limited in the ways of creating paths if you do not force it to follow an imported 3D model.

Second ABB RobotStudio programs the robot directly with a __FIXED__ path in a RAPID script. This script is run by the robot and makes the robot a _master_ of the system. This is not quite what we want in this system, we want a script or utility to control the robot and create a dynamic path at runtime. This could be implemented on the robot via RAPID script and socket programming, but it very cumbersome and requires solid knowledge about the robot architecture, making it hard to maintain.

This solution could work if the tools where made accessible over TCP from the robot. Thus requiring the operator to work in RAPID and make the control logic in RAPID. This is a strict Windows only solution.


#### Blender + Python
This stack seemed promising as Blender provides a tool for calculating the inverse kinematics as well as a framework for editing scenes in 3D. This solution is cross-platform and could provide the operator with an easy to use path planning tool.

The main issue here is getting the inverse kinematics for the robot and attach them to a 3D model of the robot. As ABB does not provide the Denavit-Hartenberg parameters for their robots this is very likely going to be an extremely imprecise simulation.  

#### Robot Operating System (ROS)
Robot Operating System (ROS) is almost considered the optimal approach. It seems to support inverse kinematics of the robot via Unified Robot Description Format (URDF). It has Point Grey camera drivers, offline programming, simulation, interprocess communication protocols, everything we need.

So whats the catch, well there doesn't exist any URDF for our particular robot model. Which means that we will have the same issue as in the Blender+Python solution. Also ROS only works on Ubuntu, not that it is an issue but it means that we cannot have both ROS and ABB RobotSuite on the computer simultaneous, of course this can be solved by using dual boot.

Another thing about ROS is that the learning curve is very steep, although they provide well written documentation. The configuration of the ROS environment is also a crucial part in the stability and thus requires much ROS knowledge of the operator, thus making the robot a difficult tool to use.


## Future Work
There is a turn table for the robot. Which in order to work only needs a driver.

Planning a path is still cumbersome and a better path planning tool is crucial for the usability of the robot.
