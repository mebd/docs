# Special Course

## Interactive Path Planning for Structured Light Scanner

ECTS | Superviser | Starting date
-----|------------|--------------
5 | Henrik Aanæs | 1. February

## Course Description
This project is about creating a web based GUI for planning the movement path for a robot arm. The robot is used to move a structured light stereo vision scanner, in a 3D space. This GUI is part of a larger software project and should ideally, in the future, be able to control other parts of the software stack.

The main goal of this project is to explore working with a software project of larger scale. How to plan and structure the works as well as which tools to use to be more productive. The importance is applying these tools and agile software development concepts in a real project. All large software projects need version control systems, in this case _git_ is used.

Minimal Viable Product would be an interactive web based GUI made with standard 3D tools for web such as ThreeJS and WebGL to create the path and visualize the robots tool in relation to the worktable and a possible workobject. The path creator should in first iteration be able make a simple path, and then when the core features are in place we expand into more complex paths.

The work of the project will end up with a written report and an oral presentation. These will document the features implemented and the core concepts and tools used in the process. The presentation should perform a live DEMO of the product as well as an overall explanation of the progress.


## Learning Objectives
In this course the student will have learned how to:

- Analyze a technical problem and extract a concrete goal
- Determine and describe suitable tools and technologies
- Work with a medium/large sized codebase
- Perform project planning, risk analysis and time management
- Work with bleeding edge web technologies
- Create an interactive 3D scene using standard tools
- Animate camera and object poses
- Use git as source code version control
- Use git-flow


## Thoughts
I will most likely be using something like Scrumban for project management. Git and git-flow for source code management.
The software stack will probably consist of NodeJS, ReactJS, Gulp, JSX, Sass (CSS3), HTML5, Foundation, ThreeJS, WebGL.
