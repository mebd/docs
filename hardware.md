# Hardware Description

## List of Hardware

- Robot
- Light controller
- 5x LED light armatures
- 2x Point Grey Grasshopper3 Cameras.
- 2x KOWA LM16SC lenses
- 1x Wintech Lightcrafter 4500 Pro projector
- Physical Tool Mount

## Robot

## Light
List of wires
Name of controller.
Documents, API etc.


## Camera
Point Grey Grasshopper3, model: GS3-U3-91S6C-C



## Projector

Wintech LightCrafter 4500 Pro


## Mount
Special made steel mount. Coated with black paint to avoid reflections.


## Arduino
I prefer writing arduino code in C++. To do this you need to use the [Bare-Arduino-Project](https://github.com/ladislas/Bare-Arduino-Project). This allows the use of C++11, makefiles and even IDEs if you're into that kind of stuff.

I have written my own stepper control library, which makes controlling the stepper more transparent.
