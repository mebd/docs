# A new robot?

If a new scanning system is to be made I have several points that should be considered.

# Robot
I would suggest a robot that does not need a cage, e.g. KUKA LBR IIWA 14 R820 or a Universal Robot UR10. As these are safe to operate around humans and easy to program.

# Lighting
A light diffuser on the camera mount to have better textures without shadows.
Another overall lighting system. This system is good for just having a fully lit scene, but what if you want to supplement measurements with shape from shading. That is not possible with current setup. I would suggest an array of consisting of LED point light sources. Preferably in a ceiling grid with 200mm space between each LED. One could also place these grids on the walls to have a more uniform light field.


# Camera
For cameras I would recommend everything but Point Grey. 9MP+ would be ideal.


# Projector
The higher the resolution the better, but other factors such as control-ability, timing wise stepping trough patterns and trigger cameras.
